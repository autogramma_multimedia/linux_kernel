﻿#ifndef _SSD254X_H__
#define _SSD254X_H__

//#define DRIVENO	19
//#define SENSENO	12

//#define ENABLE_REST_GPIO
//#define ENABLE_IRQ_GPIO

#define ENABLE_INT		0	// 0->Polling, 1->Interupt, 2->Hybrid
#define EdgeDisable		1	// if Edge Disable, set it to 1, else reset to 0
#define RunningAverageMode	2	//{0,8},{5,3},{6,2},{7,1}
#define RunningAverageDist	4	// Threshold Between two consecutive points
#define MicroTimeTInterupt	100000//10000000// 100Hz - 10,000,000us
#define FINGERNO		5

#define SCREEN_MAX_X    816 //800
#define SCREEN_MAX_Y    480 //480
#define X_OFFSET        16
#define Y_OFFSET        32
#define I2C_CTPM_ADDRESS 0x48
#define TP_CHR "tp_chr"
#define TYPE_NAME "ssd2543"

#ifdef ENABLE_REST_GPIO
#define REST_GPIO	S5PV210_GPH1(7)    //reset defined by customer
#endif

#ifdef ENABLE_IRQ_GPIO
#define IRQ			S5PV210_GPH1(6)   //irq  defined by customer
#endif

#define DEVICE_ID_REG                 2
#define VERSION_ID_REG                3
#define EVENT_STATUS                  0x79
#define FINGER01_REG                  0x7c



struct ChipSetting {
	char No;
	char Reg;
	char Data1;
	char Data2;
};



#endif


