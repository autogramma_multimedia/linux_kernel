/*
 * Copyright (C) 2015-2017 Variscite Ltd.
 * Copyright (C) 2019 Autogramma
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

&mxcfb1 {
	interface_pix_fmt = "RGB666";
	default_bpp = <18>;
	status = "okay";
};

&mxcfb2 {
	status = "disabled";
};

&mxcfb3 {
	status = "disabled";
};

&mxcfb4 {
	interface_pix_fmt = "RGB666";
	default_bpp = <18>;
	status = "okay";
};

&ecspi3 {
	/* Touch */
	ctw6120_tsc@38 {
		compatible = "edt,edt-ft5206";
		reg = <0x38>;
		interrupt-parent = <&gpio3>;
		interrupts = <7 0>;
	};
};

&ldb {
	status = "okay";

	lvds-channel@0 {
		fsl,data-mapping = "spwg";
		fsl,data-width = <18>;
		status = "okay";
		primary;

		display-timings {
			native-mode = <&timing0c>;
			timing0c: hsd100pxn1 {
				status = "okay";
				clock-frequency = <38000000>; //Dot clock (mast be 25,2 MHz)
				hactive = <640>;
				vactive = <480>;
				hback-porch  = <48>;//H back
				hfront-porch = <16>;//H front
				vback-porch  = <33>;//V back
				vfront-porch = <10>;//V front
				hsync-len = <96>;//H sync
				vsync-len = <2>; //V sync
				//H blank = H back + H sync + H front
				//Total H = H blank + H active
				//V blank = V back + V sync + V front
				//Total V = V blank + V active
				//Tclk = 1 / Dot clock
			};
		};
	};

	lvds-channel@1 {
		fsl,data-mapping = "spwg";
		fsl,data-width = <18>;
		status = "okay";

		display-timings {
			native-mode = <&timing1>;
			timing1: hsd100pxn1 {
				status = "okay";
				clock-frequency = <38000000>; //Dot clock (mast be 25,2 MHz)
				hactive = <640>;
				vactive = <480>;
				hback-porch  = <48>;//H back
				hfront-porch = <16>;//H front
				vback-porch  = <33>;//V back
				vfront-porch = <10>;//V front
				hsync-len = <96>;//H sync
				vsync-len = <2>; //V sync
				//H blank = H back + H sync + H front
				//Total H = H blank + H active
				//V blank = V back + V sync + V front
				//Total V = V blank + V active
				//Tclk = 1 / Dot clock
			};
		};
	};
};

&iomuxc
{
	imx6qdl-var-som-mx6 {
		// Variscite Uart3 support LSD Touch
		pinctrl_uart4_1: uart4grp-1 {   // RX/TX
			fsl,pins = <
				MX6QDL_PAD_KEY_ROW0__UART4_RX_DATA      0x1b0b1
				MX6QDL_PAD_KEY_COL0__UART4_TX_DATA      0x1b0b1
			>;
		};
	};
};

// ttymxc3 UART4 LCD Touch
&uart4 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_uart4_1>;
	fsl;
	status = "okay";
};
