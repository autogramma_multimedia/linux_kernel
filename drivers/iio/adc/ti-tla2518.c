/*
 * Copyright (C) 2020 Igor Modelkin <modelkiniy@gmail.com>
 *
 * Driver for Texas Instruments' TLA2518 ADC chip.
 * Datasheets can be found here:
 * https://www.ti.com/lit/ds/symlink/tla2518.pdf
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */


/*

FIXME Убрать это описание после отладки

DTS example:
&ecspi1 {
	adc_tla2518@0{
	    compatible = "ti,tla2518";
	    spi-max-frequency = <5000000>;
	    reg = <0>;
	    status = "okay";
	};
};

Как переводятся огурцы в напряжение:
напряжение на клемме = ( ( 3.3 / 65535 ) * GPIO_x_val ) * k

Значи реализованна система отключения дисплея при превышении напряжения

1) Нужно проверить путь /sys/bus/spi/drivers/tla2518/spi0.0 если эта папка есть то АЦП рабтает

2) залить настройки в дравер АЦП:
echo 1 > /sys/bus/spi/drivers/tla2518/spi0.0/AWDG_channel
echo 0 > /sys/bus/spi/drivers/tla2518/spi0.0/AWDG_ext_gpio_actlvl
echo 1 > /sys/bus/spi/drivers/tla2518/spi0.0/AWDG_above_th
echo 43 > /sys/bus/spi/drivers/tla2518/spi0.0/AWDG_ext_gpio_n
echo 43500 > /sys/bus/spi/drivers/tla2518/spi0.0/AWDG_th
echo 1 > /sys/bus/spi/drivers/tla2518/spi0.0/SAMPLE_PER

echo AIN > /sys/bus/spi/drivers/tla2518/spi0.0/GPIO_1_type
echo AIN > /sys/bus/spi/drivers/tla2518/spi0.0/GPIO_2_type

3) Получени огурцов доступно по путям:
cat /sys/bus/spi/drivers/tla2518/spi0.0/GPIO_0_val
cat /sys/bus/spi/drivers/tla2518/spi0.0/GPIO_1_val
cat /sys/bus/spi/drivers/tla2518/spi0.0/GPIO_2_val
cat /sys/bus/spi/drivers/tla2518/spi0.0/GPIO_3_val
cat /sys/bus/spi/drivers/tla2518/spi0.0/GPIO_4_val
cat /sys/bus/spi/drivers/tla2518/spi0.0/GPIO_5_val
cat /sys/bus/spi/drivers/tla2518/spi0.0/GPIO_6_val
cat /sys/bus/spi/drivers/tla2518/spi0.0/GPIO_7_val
Если порт стоит как AIN тогда отдаст значение в огурцах
Если порт стоит как DI тогда отдаст значение лог. уровня 0/1
Если порт стоит как DO тогда отдаст значение во что порт установлен 0/1

4) Надо пресчитать огурцы в напряжение это делается так:

напряжение на клемме в вольтах = ( ( 3.3 / 65535 ) * GPIO_x_val ) * k

Востановителные коэфиценты k:
GPIO_0_val  ?		(DI_H_Mx1.10_ADC)
GPIO_1_val  13.05 	(UPWR_IC_ADC)
GPIO_2_val  1.0		(NTC_ADC)
GPIO_3_val  11.02	(DI_L_Mx1.16_ADC)
GPIO_4_val  11.02	(DI_L_Mx1.15_ADC)
GPIO_5_val  11.02	(DI_L_Mx1.14_ADC)
GPIO_6_val  11.02	(DI_L_Mx1.13_ADC)
GPIO_7_val  11.02	(DI_L_Mx1.12_ADC)

в цепи UPWR_IC_ADC:
16.5В порог
echo 25000 > /sys/bus/spi/drivers/tla2518/spi0.0/AWDG_th
28.0В порог
echo 43500 > /sys/bus/spi/drivers/tla2518/spi0.0/AWDG_th
*/

#include <linux/err.h>
#include <linux/spi/spi.h>
#include <linux/module.h>
#include <linux/iio/iio.h>
#include <linux/regulator/consumer.h> 
#include <linux/of.h>
#include <linux/of_gpio.h>

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/interrupt.h>
#include <linux/kthread.h>
#include <linux/delay.h>
#include <linux/hrtimer.h>

#define GPIO_num 8

typedef enum {
    DI,
    AIN,
    DO,
}GPIO_type;

struct tla2518_sample {    
	uint16_t data;
	uint8_t channel;
};

struct tla2518_data {
	struct spi_device *spi;
	struct task_struct *task;
	struct {
	    GPIO_type type;
	    uint16_t in_val;
	    uint8_t  out_val;
	}GPIO[GPIO_num];
	struct{
	    uint8_t  channel;
	    uint16_t val_trhold;
	    uint16_t event_is_rise;
	    uint16_t disbounce;
	    int      ext_gpio_n;
	    int      ext_gpio_act_lvl;
	    struct{
		uint8_t event_fl;
		uint8_t event_fl_prv;
	    }logic;
	}An_WDG;
	int sample_period;
};

//**************** IO register func begin *************************************

static void spi_set_cs(struct spi_device *spi, int state){
	if (gpio_is_valid(spi->cs_gpio)){
		if (state != 0)
		    state = 1;
		gpio_set_value(spi->cs_gpio, state);
	}
}
static uint8_t spi_get_reg(struct spi_device *spi, uint8_t reg, int *result_ptr){
    
	int result;
	uint8_t buf[3];
	uint8_t read_data=0;
	buf[0]=0x10;
	buf[1]=reg;
	buf[2]=0x00;
	
	result = spi_write(spi, &buf, sizeof(buf));
	result = spi_read(spi, &read_data, 1);
	
	if (result_ptr != 0)
	    *result_ptr = result;
	
	return read_data;
}
static int spi_set_reg(struct spi_device *spi, uint8_t reg, uint8_t data, int *result_ptr, uint8_t write_check){    
	int result;
	uint8_t buf[3];
	uint8_t read_data=0;
	buf[0]=0x08;
	buf[1]=reg;
	buf[2]=data;
	
	result = spi_write(spi, &buf, sizeof(buf));
	
	if (write_check != 0){
		if (result != 0) goto error;
	    
		if ((read_data = spi_get_reg(spi, reg, &result)) != data)
			result = -5;
	}
	
	error:
	if (result_ptr != 0)
	    *result_ptr = result;
	
	return read_data;
}
static struct tla2518_sample spi_get_sample(struct spi_device *spi, uint8_t OSR_reg, int *result_ptr){
    
	int result;
	uint8_t buf[3];
	buf[0]=0;
	buf[1]=0;
	buf[2]=0;
	
	struct tla2518_sample sample;
	
	if (OSR_reg == 0){
	    result = spi_read(spi, &buf, 2);
	    sample.data = 0;
	    sample.data |= ((buf[0] << 4) & 0x0FF0);
	    sample.data |= ((buf[1] & 0x00F0)>>4);
	    sample.channel = (buf[1] & 0x0F);
	    
	    //dev_err(&spi->dev, "get sample buf = 0x%02x%02x data = 0x%04x, channel = 0x%02x, IO err = (%d)\n",buf[0], buf[1], sample.data, sample.channel, result);
	    
	}
	else{
	    result = spi_read(spi, &buf, 3);
	    sample.data = 0;
	    sample.data |= ((buf[0] << 8) & 0xFF00);
	    sample.data |= (buf[1] & 0x00FF) ;
	    sample.channel = (buf[2] >> 4) & 0x0F;
	    //dev_err(&spi->dev, "get sample buf = 0x%02x%02x%02x data = 0x%04x, channel = 0x%02x, IO err = (%d)\n",buf[0], buf[1], buf[2], sample.data, sample.channel, result);
	}
	
	if (result_ptr != 0)
	    *result_ptr = result;
	
	return sample;
}


//**************** IO register func end ***************************************
//**************** setup/service func begin ***********************************

static int     adc_set_setup(struct tla2518_data *adc){

	int result = 0;
	int result_io = 0;
	int i = 0;
	uint8_t DATA_CFG = 0x10;
	uint8_t OSR_CFG = 0x06;
	uint8_t PIN_CFG = 0x00;
	uint8_t GPIO_CFG = 0x00;
	uint8_t GPO_DRIVE_CFG = 0xFF; //FIXME only Push-pull driveris used for digital output supported
	uint8_t GPO_VALUE = 0x00;
	uint8_t SEQUENCE_CFG = 0x11;
	uint8_t AUTO_SEQ_CH_SEL = 0x00;
	
	for (i = 0; i < GPIO_num ;i++){
	    
	    uint8_t set_mask = (1 << i); //make 1 in pin pos;
	    uint8_t clr_mask = ~(1 << i); //make 0 in pin pos
	    
	    switch(adc->GPIO[i].type){
		case AIN:{
		    //is configuredas analog input.
		    PIN_CFG &= clr_mask; //make 0 in pin pos
		    //channel enabled in scanning sequence
		    AUTO_SEQ_CH_SEL |= set_mask; //make 1 in pin pos
		    
		}break;
		case DI:{
		    //is configured as GPIO.
		    PIN_CFG |= set_mask; //make 1 in pin pos
		    //onfigured as digital input.
		    GPIO_CFG &= clr_mask; //make 0 in pin pos
		    
		    //channel not enabled in scanning sequence
		    AUTO_SEQ_CH_SEL &= clr_mask; //make 0 in pin pos
		    
		}break;
		case DO:{
		    //is configured as GPIO.
		    PIN_CFG |= set_mask; //make 1 in pin pos
		    //configured as digital output.
		    GPIO_CFG |= set_mask; //make 1 in pin pos
		    
		    //channel not enabled in scanning sequence
		    AUTO_SEQ_CH_SEL &= clr_mask; //make 0 in pin pos
		    
		    if (adc->GPIO[i].out_val != 0)
			GPO_VALUE |= set_mask; //make 1 in pin pos
		    else
			GPO_VALUE &= clr_mask; //make 0 in pin pos
		}break;
	    }
	}
	
	spi_set_reg(adc->spi, 0x02, DATA_CFG, &result_io, 1);
	if (result_io != 0) goto io_error;
	spi_set_reg(adc->spi, 0x3, OSR_CFG, &result_io, 1);
	if (result_io != 0) goto io_error;
	spi_set_reg(adc->spi, 0x05, PIN_CFG, &result_io, 1);
	if (result_io != 0) goto io_error;
	spi_set_reg(adc->spi, 0x07, GPIO_CFG, &result_io, 1);
	if (result_io != 0) goto io_error;
	spi_set_reg(adc->spi, 0x09, GPO_DRIVE_CFG, &result_io, 1);
	if (result_io != 0) goto io_error;
	spi_set_reg(adc->spi, 0x0B, GPO_VALUE, &result_io, 1);
	if (result_io != 0) goto io_error;
	spi_set_reg(adc->spi, 0x10, SEQUENCE_CFG, &result_io, 1);
	if (result_io != 0) goto io_error;
	spi_set_reg(adc->spi, 0x12, AUTO_SEQ_CH_SEL, &result_io, 1);
	if (result_io != 0) goto io_error;
	
	return result;
	
	io_error:
	dev_err(&adc->spi->dev, "adc_set_setup io error.\n");
	
	return result;
}
static uint8_t adc_setup_is_diff(struct tla2518_data *adc, struct tla2518_data *adc_prv){
	int i = 0;
    
	for (i = 0; i < GPIO_num ;i++){
	    if (adc->GPIO[i].type != adc_prv->GPIO[i].type)
		return 1;
	    if (adc->GPIO[i].out_val != adc_prv->GPIO[i].out_val)
		return 2;
	}
	return 0;
}
static uint8_t adc_setup_have_AIN(struct tla2518_data *adc){

	int i = 0;
	int channels = 0;
	
	for (i = 0; i < GPIO_num ;i++){
	    if (adc->GPIO[i].type == AIN)
		channels++;
	}
	
	return channels;
}
static uint8_t adc_setup_have_DI(struct tla2518_data *adc){

	int i = 0;
	int channels = 0;
	
	for (i = 0; i < GPIO_num ;i++){
	    if (adc->GPIO[i].type == DI)
		channels++;
	}
	
	return channels;
}

//**************** setup/service func end *************************************
//**************** PROC FS func begin *****************************************


static ssize_t adc_GPIO_0_val_in(struct device *dev,struct device_attribute *attr,char *buf){
	struct tla2518_data *adc = dev_get_drvdata(dev);
	return scnprintf(buf, PAGE_SIZE, "%d\n", adc->GPIO[ 0 ].in_val);
}
static ssize_t adc_GPIO_0_val_out(struct device *dev, struct device_attribute *attr, const char *buf, size_t count){
	unsigned int tmp;
	struct tla2518_data *adc = dev_get_drvdata(dev);
	
	if (sscanf(buf,"%d", &tmp) == 1){
		if (tmp != 0)
		    adc->GPIO[0].out_val = 1;
		else
		    adc->GPIO[0].out_val = 0;
	}
	return count;
}
static ssize_t adc_GPIO_1_val_in(struct device *dev,struct device_attribute *attr,char *buf){
	struct tla2518_data *adc = dev_get_drvdata(dev);
	return scnprintf(buf, PAGE_SIZE, "%d\n", adc->GPIO[ 1 ].in_val);
}
static ssize_t adc_GPIO_1_val_out(struct device *dev, struct device_attribute *attr, const char *buf, size_t count){
	unsigned int tmp;
	struct tla2518_data *adc = dev_get_drvdata(dev);
	
	if (sscanf(buf,"%d", &tmp) == 1){
		if (tmp != 0)
		    adc->GPIO[1].out_val = 1;
		else
		    adc->GPIO[1].out_val = 0;
	}
	return count;
}
static ssize_t adc_GPIO_2_val_in(struct device *dev,struct device_attribute *attr,char *buf){
	struct tla2518_data *adc = dev_get_drvdata(dev);
	return scnprintf(buf, PAGE_SIZE, "%d\n", adc->GPIO[ 2 ].in_val);
}
static ssize_t adc_GPIO_2_val_out(struct device *dev, struct device_attribute *attr, const char *buf, size_t count){
	unsigned int tmp;
	struct tla2518_data *adc = dev_get_drvdata(dev);
	
	if (sscanf(buf,"%d", &tmp) == 1){
		if (tmp != 0)
		    adc->GPIO[2].out_val = 1;
		else
		    adc->GPIO[2].out_val = 0;
	}
	return count;
}
static ssize_t adc_GPIO_3_val_in(struct device *dev,struct device_attribute *attr,char *buf){
	struct tla2518_data *adc = dev_get_drvdata(dev);
	return scnprintf(buf, PAGE_SIZE, "%d\n", adc->GPIO[ 3 ].in_val);
}
static ssize_t adc_GPIO_3_val_out(struct device *dev, struct device_attribute *attr, const char *buf, size_t count){
	unsigned int tmp;
	struct tla2518_data *adc = dev_get_drvdata(dev);
	
	if (sscanf(buf,"%d", &tmp) == 1){
		if (tmp != 0)
		    adc->GPIO[3].out_val = 1;
		else
		    adc->GPIO[3].out_val = 0;
	}
	return count;
}
static ssize_t adc_GPIO_4_val_in(struct device *dev,struct device_attribute *attr,char *buf){
	struct tla2518_data *adc = dev_get_drvdata(dev);
	return scnprintf(buf, PAGE_SIZE, "%d\n", adc->GPIO[ 4 ].in_val);
}
static ssize_t adc_GPIO_4_val_out(struct device *dev, struct device_attribute *attr, const char *buf, size_t count){
	unsigned int tmp;
	struct tla2518_data *adc = dev_get_drvdata(dev);
	
	if (sscanf(buf,"%d", &tmp) == 1){
		if (tmp != 0)
		    adc->GPIO[4].out_val = 1;
		else
		    adc->GPIO[4].out_val = 0;
	}
	return count;
}
static ssize_t adc_GPIO_5_val_in(struct device *dev,struct device_attribute *attr,char *buf){
	struct tla2518_data *adc = dev_get_drvdata(dev);
	return scnprintf(buf, PAGE_SIZE, "%d\n", adc->GPIO[ 5 ].in_val);
}
static ssize_t adc_GPIO_5_val_out(struct device *dev, struct device_attribute *attr, const char *buf, size_t count){
	unsigned int tmp;
	struct tla2518_data *adc = dev_get_drvdata(dev);
	
	if (sscanf(buf,"%d", &tmp) == 1){
		if (tmp != 0)
		    adc->GPIO[5].out_val = 1;
		else
		    adc->GPIO[5].out_val = 0;
	}
	return count;
}
static ssize_t adc_GPIO_6_val_in(struct device *dev,struct device_attribute *attr,char *buf){
	struct tla2518_data *adc = dev_get_drvdata(dev);
	return scnprintf(buf, PAGE_SIZE, "%d\n", adc->GPIO[ 6 ].in_val);
}
static ssize_t adc_GPIO_6_val_out(struct device *dev, struct device_attribute *attr, const char *buf, size_t count){
	unsigned int tmp;
	struct tla2518_data *adc = dev_get_drvdata(dev);
	
	if (sscanf(buf,"%d", &tmp) == 1){
		if (tmp != 0)
		    adc->GPIO[6].out_val = 1;
		else
		    adc->GPIO[6].out_val = 0;
	}
	return count;
}
static ssize_t adc_GPIO_7_val_in(struct device *dev,struct device_attribute *attr,char *buf){
	struct tla2518_data *adc = dev_get_drvdata(dev);
	return scnprintf(buf, PAGE_SIZE, "%d\n", adc->GPIO[ 7 ].in_val);
}
static ssize_t adc_GPIO_7_val_out(struct device *dev, struct device_attribute *attr, const char *buf, size_t count){
	unsigned int tmp;
	struct tla2518_data *adc = dev_get_drvdata(dev);
	
	if (sscanf(buf,"%d", &tmp) == 1){
		if (tmp != 0)
		    adc->GPIO[7].out_val = 1;
		else
		    adc->GPIO[7].out_val = 0;
	}
	return count;
}


static ssize_t adc_GPIO_0_type_get(struct device *dev,struct device_attribute *attr,char *buf){
	struct tla2518_data *adc = dev_get_drvdata(dev);
	switch(adc->GPIO[ 0 ].type){
	    case DI :return scnprintf(buf, PAGE_SIZE, "DI (Data In)\n");
	    case AIN :return scnprintf(buf, PAGE_SIZE, "AIN (AnalogIn)\n");
	    case DO :return scnprintf(buf, PAGE_SIZE, "DO (DataOut)\n");
	};
	return 0;
}
static ssize_t adc_GPIO_0_type_set(struct device *dev, struct device_attribute *attr, const char *buf, size_t count){
	char tmp[10];
	struct tla2518_data *adc = dev_get_drvdata(dev);
	
	if (strstr(buf, "DI") != 0)
	    adc->GPIO[ 0 ].type = DI;
	else if (strstr(buf, "AIN") != 0)
	    adc->GPIO[ 0 ].type = AIN;
	else if (strstr(buf, "DO") != 0)
	    adc->GPIO[ 0 ].type = DO;
	
	return count;
}
static ssize_t adc_GPIO_1_type_get(struct device *dev,struct device_attribute *attr,char *buf){
	struct tla2518_data *adc = dev_get_drvdata(dev);
	
	switch(adc->GPIO[ 1 ].type){
	    case DI :return scnprintf(buf, PAGE_SIZE, "DI (Data In)\n");
	    case AIN :return scnprintf(buf, PAGE_SIZE, "AIN (AnalogIn)\n");
	    case DO :return scnprintf(buf, PAGE_SIZE, "DO (DataOut)\n");
	};
	return 0;
}
static ssize_t adc_GPIO_1_type_set(struct device *dev, struct device_attribute *attr, const char *buf, size_t count){
	char tmp[10];
	struct tla2518_data *adc = dev_get_drvdata(dev);
	
	if (strstr(buf, "DI") != 0)
	    adc->GPIO[ 1 ].type = DI;
	else if (strstr(buf, "AIN") != 0)
	    adc->GPIO[ 1 ].type = AIN;
	else if (strstr(buf, "DO") != 0)
	    adc->GPIO[ 1 ].type = DO;
	
	return count;
}
static ssize_t adc_GPIO_2_type_get(struct device *dev,struct device_attribute *attr,char *buf){
	struct tla2518_data *adc = dev_get_drvdata(dev);
	switch(adc->GPIO[ 2 ].type){
	    case DI :return scnprintf(buf, PAGE_SIZE, "DI (Data In)\n");
	    case AIN :return scnprintf(buf, PAGE_SIZE, "AIN (AnalogIn)\n");
	    case DO :return scnprintf(buf, PAGE_SIZE, "DO (DataOut)\n");
	};
	return 0;
}
static ssize_t adc_GPIO_2_type_set(struct device *dev, struct device_attribute *attr, const char *buf, size_t count){
	char tmp[10];
	struct tla2518_data *adc = dev_get_drvdata(dev);
	
	if (strstr(buf, "DI") != 0)
	    adc->GPIO[ 2 ].type = DI;
	else if (strstr(buf, "AIN") != 0)
	    adc->GPIO[ 2 ].type = AIN;
	else if (strstr(buf, "DO") != 0)
	    adc->GPIO[ 2 ].type = DO;
	
	return count;
}
static ssize_t adc_GPIO_3_type_get(struct device *dev,struct device_attribute *attr,char *buf){
	struct tla2518_data *adc = dev_get_drvdata(dev);
	switch(adc->GPIO[ 3 ].type){
	    case DI :return scnprintf(buf, PAGE_SIZE, "DI (Data In)\n");
	    case AIN :return scnprintf(buf, PAGE_SIZE, "AIN (AnalogIn)\n");
	    case DO :return scnprintf(buf, PAGE_SIZE, "DO (DataOut)\n");
	};
	return 0;
}
static ssize_t adc_GPIO_3_type_set(struct device *dev, struct device_attribute *attr, const char *buf, size_t count){
	char tmp[10];
	struct tla2518_data *adc = dev_get_drvdata(dev);
	
	if (strstr(buf, "DI") != 0)
	    adc->GPIO[ 3 ].type = DI;
	else if (strstr(buf, "AIN") != 0)
	    adc->GPIO[ 3 ].type = AIN;
	else if (strstr(buf, "DO") != 0)
	    adc->GPIO[ 3 ].type = DO;
	
	return count;
}
static ssize_t adc_GPIO_4_type_get(struct device *dev,struct device_attribute *attr,char *buf){
	struct tla2518_data *adc = dev_get_drvdata(dev);
	switch(adc->GPIO[ 4 ].type){
	    case DI :return scnprintf(buf, PAGE_SIZE, "DI (Data In)\n");
	    case AIN :return scnprintf(buf, PAGE_SIZE, "AIN (AnalogIn)\n");
	    case DO :return scnprintf(buf, PAGE_SIZE, "DO (DataOut)\n");
	};
	return 0;
}
static ssize_t adc_GPIO_4_type_set(struct device *dev, struct device_attribute *attr, const char *buf, size_t count){
	char tmp[10];
	struct tla2518_data *adc = dev_get_drvdata(dev);
	
	if (strstr(buf, "DI") != 0)
	    adc->GPIO[ 4 ].type = DI;
	else if (strstr(buf, "AIN") != 0)
	    adc->GPIO[ 4 ].type = AIN;
	else if (strstr(buf, "DO") != 0)
	    adc->GPIO[ 4 ].type = DO;
	
	return count;
}
static ssize_t adc_GPIO_5_type_get(struct device *dev,struct device_attribute *attr,char *buf){
	struct tla2518_data *adc = dev_get_drvdata(dev);
	switch(adc->GPIO[ 5 ].type){
	    case DI :return scnprintf(buf, PAGE_SIZE, "DI (Data In)\n");
	    case AIN :return scnprintf(buf, PAGE_SIZE, "AIN (AnalogIn)\n");
	    case DO :return scnprintf(buf, PAGE_SIZE, "DO (DataOut)\n");
	};
	return 0;
}
static ssize_t adc_GPIO_5_type_set(struct device *dev, struct device_attribute *attr, const char *buf, size_t count){
	char tmp[10];
	struct tla2518_data *adc = dev_get_drvdata(dev);
	
	if (strstr(buf, "DI") != 0)
	    adc->GPIO[ 5 ].type = DI;
	else if (strstr(buf, "AIN") != 0)
	    adc->GPIO[ 5 ].type = AIN;
	else if (strstr(buf, "DO") != 0)
	    adc->GPIO[ 5 ].type = DO;
	
	return count;
}
static ssize_t adc_GPIO_6_type_get(struct device *dev,struct device_attribute *attr,char *buf){
	struct tla2518_data *adc = dev_get_drvdata(dev);
	switch(adc->GPIO[ 6 ].type){
	    case DI :return scnprintf(buf, PAGE_SIZE, "DI (Data In)\n");
	    case AIN :return scnprintf(buf, PAGE_SIZE, "AIN (AnalogIn)\n");
	    case DO :return scnprintf(buf, PAGE_SIZE, "DO (DataOut)\n");
	};
	return 0;
}
static ssize_t adc_GPIO_6_type_set(struct device *dev, struct device_attribute *attr, const char *buf, size_t count){
	char tmp[10];
	struct tla2518_data *adc = dev_get_drvdata(dev);
	
	if (strstr(buf, "DI") != 0)
	    adc->GPIO[ 6 ].type = DI;
	else if (strstr(buf, "AIN") != 0)
	    adc->GPIO[ 6 ].type = AIN;
	else if (strstr(buf, "DO") != 0)
	    adc->GPIO[ 6 ].type = DO;
	
	return count;
}
static ssize_t adc_GPIO_7_type_get(struct device *dev,struct device_attribute *attr,char *buf){
	struct tla2518_data *adc = dev_get_drvdata(dev);
	switch(adc->GPIO[ 7 ].type){
	    case DI :return scnprintf(buf, PAGE_SIZE, "DI (Data In)\n");
	    case AIN :return scnprintf(buf, PAGE_SIZE, "AIN (AnalogIn)\n");
	    case DO :return scnprintf(buf, PAGE_SIZE, "DO (DataOut)\n");
	};
	return 0;
}
static ssize_t adc_GPIO_7_type_set(struct device *dev, struct device_attribute *attr, const char *buf, size_t count){
	char tmp[10];
	struct tla2518_data *adc = dev_get_drvdata(dev);
	
	if (strstr(buf, "DI") != 0)
	    adc->GPIO[ 7 ].type = DI;
	else if (strstr(buf, "AIN") != 0)
	    adc->GPIO[ 7 ].type = AIN;
	else if (strstr(buf, "DO") != 0)
	    adc->GPIO[ 7 ].type = DO;
	
	return count;
}
static ssize_t adc_Sample_per_get(struct device *dev,struct device_attribute *attr,char *buf){
	struct tla2518_data *adc = dev_get_drvdata(dev);
	return scnprintf(buf, PAGE_SIZE, "%d\n", adc->sample_period);
}
static ssize_t adc_Sample_per_set(struct device *dev, struct device_attribute *attr, const char *buf, size_t count){
	unsigned int tmp;
	struct tla2518_data *adc = dev_get_drvdata(dev);
	
	if (sscanf(buf,"%d", &tmp) == 1)
	    adc->sample_period = tmp;
	return count;
}

static ssize_t adc_AWDG_th_get(struct device *dev,struct device_attribute *attr,char *buf){
	struct tla2518_data *adc = dev_get_drvdata(dev);
	return scnprintf(buf, PAGE_SIZE, "%d\n", adc->An_WDG.val_trhold);
}
static ssize_t adc_AWDG_th_set(struct device *dev, struct device_attribute *attr, const char *buf, size_t count){
	unsigned int tmp;
	struct tla2518_data *adc = dev_get_drvdata(dev);
	
	if (sscanf(buf,"%d", &tmp) == 1)
	    adc->An_WDG.val_trhold = tmp;
	return count;
}
static ssize_t adc_AWDG_ch_get(struct device *dev,struct device_attribute *attr,char *buf){
	struct tla2518_data *adc = dev_get_drvdata(dev);
	return scnprintf(buf, PAGE_SIZE, "%d\n", adc->An_WDG.channel);
}
static ssize_t adc_AWDG_ch_set(struct device *dev, struct device_attribute *attr, const char *buf, size_t count){
	unsigned int tmp;
	struct tla2518_data *adc = dev_get_drvdata(dev);
	
	if (sscanf(buf,"%d", &tmp) == 1)
	    if (tmp >= 0 && tmp <= 7)
		adc->An_WDG.channel = tmp;
	return count;
}
static ssize_t adc_AWDG_above_get(struct device *dev,struct device_attribute *attr,char *buf){
	struct tla2518_data *adc = dev_get_drvdata(dev);
	return scnprintf(buf, PAGE_SIZE, "%d\n", adc->An_WDG.event_is_rise);
}
static ssize_t adc_AWDG_above_set(struct device *dev, struct device_attribute *attr, const char *buf, size_t count){
	unsigned int tmp;
	struct tla2518_data *adc = dev_get_drvdata(dev);
	
	if (sscanf(buf,"%d", &tmp) == 1)
	    adc->An_WDG.event_is_rise = ((tmp != 0)?1:0);
	return count;
}
static ssize_t adc_AWDG_ext_gpio_get(struct device *dev,struct device_attribute *attr,char *buf){
	struct tla2518_data *adc = dev_get_drvdata(dev);
	return scnprintf(buf, PAGE_SIZE, "%d\n", adc->An_WDG.ext_gpio_n);
}
static ssize_t adc_AWDG_ext_gpio_set(struct device *dev, struct device_attribute *attr, const char *buf, size_t count){
	unsigned int tmp;
	struct tla2518_data *adc = dev_get_drvdata(dev);
	
	if (sscanf(buf,"%d", &tmp) == 1)
	    adc->An_WDG.ext_gpio_n = tmp;
	return count;
}
static ssize_t adc_AWDG_ext_gpio_lvl_get(struct device *dev,struct device_attribute *attr,char *buf){
	struct tla2518_data *adc = dev_get_drvdata(dev);
	return scnprintf(buf, PAGE_SIZE, "%d\n", adc->An_WDG.ext_gpio_act_lvl);
}
static ssize_t adc_AWDG_ext_gpio_lvl_set(struct device *dev, struct device_attribute *attr, const char *buf, size_t count){
	unsigned int tmp;
	struct tla2518_data *adc = dev_get_drvdata(dev);
	
	if (sscanf(buf,"%d", &tmp) == 1)
	    adc->An_WDG.ext_gpio_act_lvl = ((tmp != 0)?1:0);
	return count;
}


static ssize_t spi_dbg_read_reg  (struct device *dev, struct device_attribute *attr, const char *buf, size_t count){
	unsigned int adr;
	struct spi_device *spi = to_spi_device(dev);
	
	if (sscanf(buf,"%d", &adr) != 1)
		dev_err(&spi->dev, "Wrong arg. [adr]\n");
	else if (adr >= 0 && adr <= 255){
		int err = 0;
		uint8_t result = spi_get_reg(spi, adr, &err);
		dev_err(&spi->dev, "Read reg adr = 0x%02x, val = 0x%02x, IO err = (%d)\n",adr, result, err);
	}
	else
		dev_err(&spi->dev, "Wrong arg. Reg adr mast be num from 0 to 255 .\n");
	return count;
}
static ssize_t spi_dbg_write_reg  (struct device *dev, struct device_attribute *attr, const char *buf, size_t count){
	unsigned int adr, val;
	struct spi_device *spi = to_spi_device(dev);
	
	if (sscanf(buf,"%d %d", &adr, &val) != 2)
		dev_err(&spi->dev, "Wrong arg. [adr] [val].\n");
	else if ((adr >= 0 && adr <= 255) &&  (val >= 0 && val <= 255)){
		int err = 0;
		spi_set_reg(spi, adr, val, &err, 1);
		dev_err(&spi->dev, "Write reg adr = 0x%02x, val = 0x%02x, IO err = (%d)\n",adr, val, err);
	}
	else
		dev_err(&spi->dev, "Wrong arg. Reg adr & val mast be num from 0 to 255 .\n");
	
	return count;
}
static ssize_t spi_dbg_read_smpl  (struct device *dev, struct device_attribute *attr, const char *buf, size_t count){
	unsigned int adr;
	struct spi_device *spi = to_spi_device(dev);
	
	spi_get_sample(spi, spi_get_reg(spi, 3, 0), 0);
	
	return count;
}


static DEVICE_ATTR(GPIO_0_val,		S_IWUSR|S_IRUSR, adc_GPIO_0_val_in,  adc_GPIO_0_val_out);
static DEVICE_ATTR(GPIO_1_val,		S_IWUSR|S_IRUSR, adc_GPIO_1_val_in,  adc_GPIO_1_val_out);
static DEVICE_ATTR(GPIO_2_val,		S_IWUSR|S_IRUSR, adc_GPIO_2_val_in,  adc_GPIO_2_val_out);
static DEVICE_ATTR(GPIO_3_val,		S_IWUSR|S_IRUSR, adc_GPIO_3_val_in,  adc_GPIO_3_val_out);
static DEVICE_ATTR(GPIO_4_val,		S_IWUSR|S_IRUSR, adc_GPIO_4_val_in,  adc_GPIO_4_val_out);
static DEVICE_ATTR(GPIO_5_val,		S_IWUSR|S_IRUSR, adc_GPIO_5_val_in,  adc_GPIO_5_val_out);
static DEVICE_ATTR(GPIO_6_val,		S_IWUSR|S_IRUSR, adc_GPIO_6_val_in,  adc_GPIO_6_val_out);
static DEVICE_ATTR(GPIO_7_val,		S_IWUSR|S_IRUSR, adc_GPIO_7_val_in,  adc_GPIO_7_val_out);

static DEVICE_ATTR(GPIO_0_type,		S_IWUSR|S_IRUSR, adc_GPIO_0_type_get,  adc_GPIO_0_type_set);
static DEVICE_ATTR(GPIO_1_type,		S_IWUSR|S_IRUSR, adc_GPIO_1_type_get,  adc_GPIO_1_type_set);
static DEVICE_ATTR(GPIO_2_type,		S_IWUSR|S_IRUSR, adc_GPIO_2_type_get,  adc_GPIO_2_type_set);
static DEVICE_ATTR(GPIO_3_type,		S_IWUSR|S_IRUSR, adc_GPIO_3_type_get,  adc_GPIO_3_type_set);
static DEVICE_ATTR(GPIO_4_type,		S_IWUSR|S_IRUSR, adc_GPIO_4_type_get,  adc_GPIO_4_type_set);
static DEVICE_ATTR(GPIO_5_type,		S_IWUSR|S_IRUSR, adc_GPIO_5_type_get,  adc_GPIO_5_type_set);
static DEVICE_ATTR(GPIO_6_type,		S_IWUSR|S_IRUSR, adc_GPIO_6_type_get,  adc_GPIO_6_type_set);
static DEVICE_ATTR(GPIO_7_type,		S_IWUSR|S_IRUSR, adc_GPIO_7_type_get,  adc_GPIO_7_type_set);
static DEVICE_ATTR(SAMPLE_PER,		S_IWUSR|S_IRUSR, adc_Sample_per_get,  adc_Sample_per_set);

static DEVICE_ATTR(AWDG_th,		S_IWUSR|S_IRUSR, adc_AWDG_th_get,  adc_AWDG_th_set);
static DEVICE_ATTR(AWDG_channel,	S_IWUSR|S_IRUSR, adc_AWDG_ch_get,  adc_AWDG_ch_set);
static DEVICE_ATTR(AWDG_above_th,	S_IWUSR|S_IRUSR, adc_AWDG_above_get,  adc_AWDG_above_set);
static DEVICE_ATTR(AWDG_ext_gpio_n,	S_IWUSR|S_IRUSR, adc_AWDG_ext_gpio_get,  adc_AWDG_ext_gpio_set);
static DEVICE_ATTR(AWDG_ext_gpio_actlvl,S_IWUSR|S_IRUSR, adc_AWDG_ext_gpio_lvl_get,  adc_AWDG_ext_gpio_lvl_set);

static DEVICE_ATTR(read_reg,		S_IWUSR, 0, spi_dbg_read_reg);
static DEVICE_ATTR(write_reg,		S_IWUSR, 0, spi_dbg_write_reg);
static DEVICE_ATTR(read_sample,		S_IWUSR, 0, spi_dbg_read_smpl);


static struct attribute *spi_adc_attributes[] = {
	&dev_attr_GPIO_0_val.attr,
	&dev_attr_GPIO_1_val.attr,
	&dev_attr_GPIO_2_val.attr,
	&dev_attr_GPIO_3_val.attr,
	&dev_attr_GPIO_4_val.attr,
	&dev_attr_GPIO_5_val.attr,
	&dev_attr_GPIO_6_val.attr,
	&dev_attr_GPIO_7_val.attr,
	&dev_attr_GPIO_0_type.attr,
	&dev_attr_GPIO_1_type.attr,
	&dev_attr_GPIO_2_type.attr,
	&dev_attr_GPIO_3_type.attr,
	&dev_attr_GPIO_4_type.attr,
	&dev_attr_GPIO_5_type.attr,
	&dev_attr_GPIO_6_type.attr,
	&dev_attr_GPIO_7_type.attr,
	
	&dev_attr_SAMPLE_PER.attr,
	&dev_attr_AWDG_th.attr,
	&dev_attr_AWDG_channel.attr,
	&dev_attr_AWDG_above_th.attr,
	&dev_attr_AWDG_ext_gpio_n.attr,
	&dev_attr_AWDG_ext_gpio_actlvl.attr,
	&dev_attr_read_reg.attr,
	&dev_attr_write_reg.attr,
	&dev_attr_read_sample.attr,
	NULL
};

static const struct attribute_group spi_adc_group = {
	.attrs = spi_adc_attributes,
};

//**************** PROC FS func end *******************************************

int adc_thread(void *data){
    struct tla2518_data *adc = data;
    struct tla2518_data *adc_prv;
    int result = 0;
    int i = 0;
    
    adc_prv = kzalloc(sizeof(struct tla2518_data), GFP_KERNEL);
    if (!adc_prv){
	dev_err(&adc->spi->dev, "Work thread mem alloc error\n");
	return -ENOMEM;
    }
    
    result = adc_set_setup(adc);
    
    
    if (result != 0)
	goto io_error;
    
    memcpy(adc_prv, adc, sizeof(struct tla2518_data));
    
    dev_err(&adc->spi->dev, "Work thread started\n");
    
    while(!kthread_should_stop()){
	uint8_t sample_fl = 0;
	
	if (adc_setup_is_diff(adc, adc_prv) != 0){
		dev_err(&adc->spi->dev, "Set new setup\n");
		result = adc_set_setup(adc);
		
		if (result != 0)
		    goto io_error;
		
		memcpy(adc_prv, adc, sizeof(struct tla2518_data));
	}
	
	if (adc_setup_have_AIN(adc) != 0){
	    
	    sample_fl++;
	    
	    for (i=0; i < adc_setup_have_AIN(adc); i++){
		struct tla2518_sample sample = spi_get_sample(adc->spi, 0x06, &result);
	    
	    
		if (sample.channel > (GPIO_num - 1)){
			dev_err(&adc->spi->dev, "Sample data get unexpect channel num = %d\n", sample.channel);
		    goto io_error;
		}
		
		adc->GPIO[sample.channel].in_val = sample.data;
		
	    
		if (result != 0)
		    goto io_error;
	    }
	    
	    if ((adc->GPIO[adc->An_WDG.channel].type == AIN) && (adc->An_WDG.val_trhold != 0)){
		
		//make triger/relise event
		if (adc->An_WDG.event_is_rise == 1){
		    if (adc->An_WDG.val_trhold <= adc->GPIO[adc->An_WDG.channel].in_val){
			adc->An_WDG.logic.event_fl = 1;
		    }
		    else{
			adc->An_WDG.logic.event_fl = 0;
		    }
		}
		else{
		    if (adc->An_WDG.val_trhold >= adc->GPIO[adc->An_WDG.channel].in_val)
			adc->An_WDG.logic.event_fl = 1;
		    else
			adc->An_WDG.logic.event_fl = 0;
		}
		
		
		//toggle gpio
		if (adc->An_WDG.logic.event_fl > adc->An_WDG.logic.event_fl_prv){
		    
		    dev_err(&adc->spi->dev, "Analog WDG - event detected! \n");
		    
		    if (gpio_is_valid(adc->An_WDG.ext_gpio_n) == 1)
			gpio_direction_output(adc->An_WDG.ext_gpio_n, adc->An_WDG.ext_gpio_act_lvl);
		    else
			dev_err(&adc->spi->dev, "Analog WDG - triggered, but event GPIO(%d) uncorrect!\n", adc->An_WDG.ext_gpio_n);
		}
		if (adc->An_WDG.logic.event_fl < adc->An_WDG.logic.event_fl_prv){
		    
		    dev_err(&adc->spi->dev, "Analog WDG - event is done! \n");
		    
		    if (gpio_is_valid(adc->An_WDG.ext_gpio_n) == 1)
			gpio_direction_output(adc->An_WDG.ext_gpio_n, !adc->An_WDG.ext_gpio_act_lvl);
		    else
			dev_err(&adc->spi->dev, "Analog WDG - triggered, but event GPIO(%d) uncorrect!\n", adc->An_WDG.ext_gpio_n);
		    
		}
		adc->An_WDG.logic.event_fl_prv = adc->An_WDG.logic.event_fl;
	    }
	
	    
	}
	
	if (adc_setup_have_DI(adc) != 0){
	    uint8_t gpio_val = spi_get_reg(adc->spi, 0x0D, 0);
	    
	    sample_fl++;
	    
	    for (i = 0; i < GPIO_num ;i++){
		if (adc->GPIO[i].type == DI)
		    adc->GPIO[i].in_val = ((gpio_val >> i) & 0x01);
	    }
	}
	
	if (sample_fl != 0){
	
	    if (adc->sample_period < 1)
		adc->sample_period = 1;
	    if (adc->sample_period > 1000)
		adc->sample_period = 1000;
		
	    if (adc->sample_period < 100){
		int usleep = adc->sample_period * 1000;
		usleep_range(usleep-100, usleep);//стабильно работает с малыми интервалам.
	    }
	    else
		msleep(adc->sample_period-10);//не так стабилен но его рекомендуют использовать.
	}
	else{
	    msleep(100);
	}

    }

    
    dev_err(&adc->spi->dev, "adc_set_setup io error.\n");
    return 0;
    
    io_error:
    dev_err(&adc->spi->dev, "adc_set_setup io error.\n");
    return result;
}




static int tla2518_probe(struct spi_device *spi){
	struct tla2518_data *adc;
	int ret, ret2;

	//warm up data/memory	
	adc = kzalloc(sizeof(struct tla2518_data), GFP_KERNEL);
	if (!adc)
	    return -ENOMEM;
	
	dev_set_drvdata(&spi->dev, adc);
	dev_err(&adc->spi->dev, "Start probe\n");
	
	adc->spi = spi;	
	adc->spi->bits_per_word = 8;
	adc->spi->mode = SPI_MODE_0;
	adc->spi->max_speed_hz = 5000000;
	
	ret = spi_setup(adc->spi);
	
	if (ret < 0) {
		dev_err(&spi->dev, "spi_setup failed: %d\n", ret);
		goto io_error;
	}
	
	//CS test
	if (gpio_is_valid(adc->spi->cs_gpio) == 0)
		goto cs_error;
	
	gpio_direction_output(adc->spi->cs_gpio, 1);
	
	//IO test
	ret2 = spi_set_reg(adc->spi, 0x01, 0x01, &ret, 0); // reset device
	
	if (ret < 0) {
		dev_err(&spi->dev, "failed to set register %d, data %d, read data %d: %d", 0x01, 0x01, ret2, ret);
		goto io_error;
	}
	
	msleep(20);
	if ((ret2 = spi_get_reg(adc->spi, 0x00, &ret)) != 0x81) {
	    dev_err(&adc->spi->dev, "No correct state answer. Got %d, expected %d, ret %d\n", ret2, 0x81, ret);
	    goto io_error;
	}
	
	//make proc sf
	ret = sysfs_create_group(&adc->spi->dev.kobj, &spi_adc_group);
	if(ret < 0) {
		dev_err(&spi->dev, "failed to create sysfs group: %d", ret);
		kzfree(adc);
		return ret;
	}
	
	//make thread
	adc->task = kthread_run(&adc_thread,(void *)adc,"ADC_tla2518");
	
	//return result
	dev_err(&adc->spi->dev, "tla2518_probe: result: %d\n", ret);
	return ret;
	
	//return falure states:
	cs_error:
	dev_err(&adc->spi->dev, "Probe falure, Cip Select falure.\n");
	kzfree(adc);
	return -19;
	
	io_error:
	dev_err(&adc->spi->dev, "Probe falure, I/O error.\n");
	kzfree(adc);
	return -5;
	
}
static int tla2518_remove(struct spi_device *spi){
    

	sysfs_remove_group(&spi->dev.kobj, &spi_adc_group);
	dev_err(&spi->dev, "tla2518_remove: start remove\n");
	
	return 0;
}

//*********************************************************************


static const struct of_device_id tla2518_of_match[] = {
	{ .compatible = "ti,tla2518", },
	{ /* sentinel */ },
};
MODULE_DEVICE_TABLE(of, tla2518_of_match);

static const struct spi_device_id tla2518_id[] = {
	{ "tla2518", 0},	/* index into tla2518_config */
	{ }
};
MODULE_DEVICE_TABLE(spi, tla2518_id);

static struct spi_driver tla2518_driver = {
	.driver = {
		.name = "tla2518",
		.of_match_table = of_match_ptr(tla2518_of_match),
	},
	.probe = tla2518_probe,
	.remove = tla2518_remove,
	.id_table = tla2518_id,
};
module_spi_driver(tla2518_driver);

MODULE_AUTHOR("Igor Modelkin <modelkiniy@gmail.com>");
MODULE_DESCRIPTION("Texas Instruments TLA2518");
MODULE_LICENSE("GPL v2");
