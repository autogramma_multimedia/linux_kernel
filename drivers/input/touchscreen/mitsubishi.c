#include <linux/errno.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/input.h>
#include <linux/serio.h>
#include <linux/init.h>
#include <linux/ctype.h>

#define DRIVER_DESC	"MITSUBISHI UART Touch driver"
#define DEBUG

MODULE_AUTHOR("Fumihiro Atsumi <atsumi@pylone.jp>");
MODULE_DESCRIPTION(DRIVER_DESC);
MODULE_LICENSE("GPL");

#define	MITSUBISHI_MAX_LENGTH	16
#define MITSUBISHI_MAX_FINGERS	2
#define MITSUBISHI_MAX_TRKID	(MITSUBISHI_MAX_FINGERS - 1)
#define MITSUBISHI_MAX_X		4095
#define MITSUBISHI_MAX_Y		4095

#define MITSUBISHI_HEAD_MASK	0xF0
#define MITSUBISHI_HEAD_BYTE	0xD0

#define MITSUBISHI_REQ_VERSION	0xA5
#define MITSUBISHI_REQ_RESET	0xAF

#define MITSUBISHI_SET_SAMPRATE	0xA2

#define MITSUBISHI_ACK_VERSION	0xD5
#define MITSUBISHI_ACK_RESET	0xDF
#define MITSUBISHI_ACK_SAMPRATE	0xD2
#define MITSUBISHI_FIN_INIT		0xD9
#define MITSUBISHI_INF_ERROR	0xDA
#define MITSUBISHI_INF_TOUCH	0xDC

#define MITSUBISHI_DEBUG_DEFAULT	MITSUBISHI_DEBUG_MSG_SATE

#define MITSUBISHI_DEBUG_MSG_SATE	0x01
#define MITSUBISHI_DEBUG_MSG_LOG	0x02
#define MITSUBISHI_DEBUG_MSG_RAWDATA	0x03
#define MITSUBISHI_DEBUG_MSG_ALLDATA	0x04

/*
 * Per-touchscreen data.
 */

struct mitsubishi_finger {
	s32 x, y;
	s32 contactid;
	bool touch_state;
};

struct mitsubishi_info {
	struct mitsubishi_verion {
		u8 major;
		u8 minor;
	} version;
};

struct mitsubishi {
	struct input_dev *dev;
	struct serio *serio;
	struct mutex reset_mutex;
	struct mutex cmd_mutex;
	struct completion init_done;
	struct completion cmd_done;
	unsigned char chksum;
	unsigned char len;
	int idx;
	int count;
	int hw_initialized;
	unsigned char response_type;
	unsigned char response[MITSUBISHI_MAX_LENGTH];
	unsigned char data[MITSUBISHI_MAX_LENGTH];
	unsigned long debug_flags;
	unsigned long semp_rate;
	char phys[32];
	struct mitsubishi_info info;
	struct mitsubishi_finger f[MITSUBISHI_MAX_FINGERS];
};

static ssize_t mitsubishi_show_version(struct device *dev,
				   struct device_attribute *attr, char *buf)
{
	struct serio *serio = to_serio_port(dev);
	struct mitsubishi *mitsubishi = serio_get_drvdata(serio);

	return sprintf(buf, "v%X.%X\n",
		       mitsubishi->info.version.major, mitsubishi->info.version.minor);
}

static int mitsubishi_setup(struct mitsubishi *mitsubishi);

static ssize_t mitsubishi_store_reset(struct device *dev,
				  struct device_attribute *attr,
				  const char *buf, size_t count)
{
	struct serio *serio = to_serio_port(dev);
	struct mitsubishi *mitsubishi = serio_get_drvdata(serio);
	int rc;

	rc = mitsubishi_setup(mitsubishi);
	if (rc)
		return rc;

	return count;
}

static ssize_t mitsubishi_show_debug(struct device *dev,
				 struct device_attribute *attr, char *buf)
{
	struct serio *serio = to_serio_port(dev);
	struct mitsubishi *mitsubishi = serio_get_drvdata(serio);

	return sprintf(buf, "%i\n", mitsubishi->debug_flags);
}

static ssize_t mitsubishi_store_debug(struct device *dev,
				  struct device_attribute *attr,
				  const char *buf, size_t count)
{
	struct serio *serio = to_serio_port(dev);
	struct mitsubishi *mitsubishi = serio_get_drvdata(serio);

	sscanf(buf, "%i", &mitsubishi->debug_flags);
	return count;
}


static void report_touch_events(struct mitsubishi *mitsubishi)
{
	struct input_dev *dev = mitsubishi->dev;
	int count = 0;
	int i;

	for (i = 0; i < MITSUBISHI_MAX_FINGERS; i++) {
		struct mitsubishi_finger *f = &mitsubishi->f[i];
		
		
		if (mitsubishi->debug_flags >= MITSUBISHI_DEBUG_MSG_RAWDATA)
		  printk(KERN_INFO "Touch: X:%i Y:%i Tr:%i State:%i\n", f->x, f->y, i, f->touch_state);

		if (f->touch_state) {
			input_report_key(dev, BTN_TOUCH, 1);
			input_report_abs(dev, ABS_MT_TRACKING_ID, i);
			input_report_abs(dev, ABS_MT_POSITION_X, f->x);
			input_report_abs(dev, ABS_MT_POSITION_Y, f->y);
			input_report_abs(dev, ABS_X, f->x);
			input_report_abs(dev, ABS_Y, f->y);
			input_mt_sync(dev);
			count++;
		} else {
			input_report_key(dev, BTN_TOUCH, 0);
		}
	}

	if (!count)
		input_mt_sync(dev);

	input_sync(dev);
}

static void parse_touch_data(struct mitsubishi *mitsubishi)
{
	int id;
	bool valid, delimiter;
	struct mitsubishi_finger *f;

	if (unlikely(!mitsubishi->hw_initialized))
		return;

	id = (mitsubishi->data[3] >> 2) & 0x0F;
	if (id < MITSUBISHI_MAX_FINGERS) {
		valid = (mitsubishi->data[3] >> 7) & 0x01;
		delimiter = (mitsubishi->data[3] >> 1) & 0x01;
		if (valid) {
			f = &mitsubishi->f[id];
			f->touch_state = (mitsubishi->data[3] >> 0) & 0x01;
			f->x = (u16)mitsubishi->data[4] << 6 | mitsubishi->data[5];
			f->y = (u16)mitsubishi->data[6] << 6 | mitsubishi->data[7];
			mitsubishi->count++;
		}
		if (delimiter && mitsubishi->count > 0) {
			report_touch_events(mitsubishi);
			mitsubishi->count = 0;
		}
	} else
		printk(KERN_WARNING "mitsubishi: invalid finger ID (%d)\n", id);
}

static void dump_data(struct mitsubishi *mitsubishi)
{
	int i;
	int len = mitsubishi->data[1] + 2;

	printk(KERN_INFO "Touch: Data dump");
	
	for (i = 0; i < len; i++) {
		printk("0x%02X ", mitsubishi->data[i]);

		if (i != len -1)
			printk(" ");
		else
			printk("\n");
	}
}

static void parse_data(struct mitsubishi *mitsubishi)
{
	if (mitsubishi->debug_flags >= MITSUBISHI_DEBUG_MSG_ALLDATA)
		dump_data(mitsubishi);

	switch (mitsubishi->data[0]) {
	case MITSUBISHI_INF_TOUCH:
		parse_touch_data(mitsubishi);
		break;
	case MITSUBISHI_ACK_RESET:
		mitsubishi->response_type = MITSUBISHI_ACK_RESET;
		complete(&mitsubishi->cmd_done);
		break;
	case MITSUBISHI_FIN_INIT:
		complete(&mitsubishi->init_done);
		break;
	case MITSUBISHI_ACK_VERSION:
		mitsubishi->response_type = MITSUBISHI_ACK_VERSION;
		memcpy(mitsubishi->response, mitsubishi->data, MITSUBISHI_MAX_LENGTH);
		complete(&mitsubishi->cmd_done);
		break;
	case MITSUBISHI_INF_ERROR:
		mitsubishi->response_type = MITSUBISHI_INF_ERROR;
		memcpy(mitsubishi->response, mitsubishi->data, MITSUBISHI_MAX_LENGTH);
		complete(&mitsubishi->cmd_done);
		break;
		
	case MITSUBISHI_ACK_SAMPRATE:
		mitsubishi->response_type = MITSUBISHI_ACK_SAMPRATE;
		complete(&mitsubishi->cmd_done);
		break;
		
	default:
		printk(KERN_WARNING
		       "mitsubishi: unknown data (0x%02X)\n",
		       mitsubishi->data[0]);
		break;
	}
}

static irqreturn_t mitsubishi_interrupt(struct serio *serio,
		unsigned char data, unsigned int flags)
{
	struct mitsubishi* mitsubishi = serio_get_drvdata(serio);
	
	if (mitsubishi->debug_flags >= MITSUBISHI_DEBUG_MSG_ALLDATA)
	  printk(KERN_INFO "Touch: IRQ\n");
	
	if (mitsubishi->idx == 0) {
		if ((data & MITSUBISHI_HEAD_MASK) != MITSUBISHI_HEAD_BYTE)
			return IRQ_HANDLED;
	} else {
		if ((data & MITSUBISHI_HEAD_MASK) == MITSUBISHI_HEAD_BYTE)
			mitsubishi->idx = 0;
	}

	mitsubishi->data[mitsubishi->idx] = data;

	switch (mitsubishi->idx) {
	case 0:
		mitsubishi->chksum = data;
		mitsubishi->idx++;
		break;
	case 1:
		if (data > 0 && data < MITSUBISHI_MAX_LENGTH - 1) {
			mitsubishi->len = data;
			mitsubishi->chksum += data;
			mitsubishi->idx++;
		} else
			mitsubishi->idx = 0;
		break;
	default:
		if (mitsubishi->idx - 1 < mitsubishi->len) {
			mitsubishi->chksum += data;
			mitsubishi->idx++;
		} else {
			if (data == (mitsubishi->chksum & 0x7F))
				parse_data(mitsubishi);
			else
				printk(KERN_WARNING
				       "mitsubishi: invalid checksum (0x%02X,0x%02X)\n",
				       data, mitsubishi->chksum & 0x7F);
			mitsubishi->idx = 0;
		}
		break;
	}

	return IRQ_HANDLED;
}

struct mitsubishi_packet {
	u8 header;
	u8 data_len;
	u8 data[MITSUBISHI_MAX_LENGTH - 2];
} __attribute__ ((packed));

static int mitsubishi_command(struct mitsubishi *mitsubishi, struct mitsubishi_packet *pkt,
			  struct mitsubishi_packet *resp, unsigned int timeout)
{
	unsigned long timeleft;
	int i;
	int rc;
	
	if (mitsubishi->debug_flags >= MITSUBISHI_DEBUG_MSG_ALLDATA){
	  printk(KERN_INFO "Touch: TX CMD = 0x%02X 0x%02X", pkt->header, pkt->data_len);
	  for (i = 0; i < pkt->data_len; i++) {
		  printk("0x%02X ", pkt->data[i]);

		  if (i != pkt->data_len - 1 )
			  printk(" ");
		  else
			  printk("\n");
	  }
	}

	mutex_lock(&mitsubishi->cmd_mutex);

	mitsubishi->response_type = 0;
	init_completion(&mitsubishi->cmd_done);

	rc = serio_write(mitsubishi->serio, pkt->header);
	if (rc)
		goto out1;

	rc = serio_write(mitsubishi->serio, pkt->data_len);
	if (rc)
		goto out1;

	for (i = 0; i < pkt->data_len; i++) {
		rc = serio_write(mitsubishi->serio, pkt->data[i]);
		if (rc)
			goto out1;
	}

	timeleft = wait_for_completion_timeout(&mitsubishi->cmd_done,
					       msecs_to_jiffies(timeout));
	if (timeleft == 0) {
		printk(KERN_WARNING
		       "mitsubishi: timeout waiting for command response (0x%02X)\n",
		       pkt->header);
		rc = -ETIMEDOUT;
		goto out1;
	}

	if (mitsubishi->response_type == MITSUBISHI_INF_ERROR) {
		printk(KERN_WARNING
		       "mitsubishi: recieved error response (0x%02X,0x%02X)\n",
		       mitsubishi->response[2], mitsubishi->response[3]);
		rc = -EIO;
		goto out2;
	}

	if ((pkt->header & 0x0F) != (mitsubishi->response_type & 0x0F)) {
		printk(KERN_WARNING
		       "mitsubishi: unexpected response type (0x%02X,0x%02X)\n",
		       pkt->header, mitsubishi->response_type);
		rc = -EIO;
		goto out2;
	}

out2:
	if (resp)
		memcpy((unsigned char *)resp, mitsubishi->response, MITSUBISHI_MAX_LENGTH);
out1:
	mutex_unlock(&mitsubishi->cmd_mutex);
	return rc;
}

static int mitsubishi_sw_reset(struct mitsubishi *mitsubishi)
{
	struct mitsubishi_packet pkt;
	unsigned long timeleft;
	int rc = 0;
	
	if (mitsubishi->debug_flags >= MITSUBISHI_DEBUG_MSG_LOG)
	  printk(KERN_INFO "Touch: device reset\n");

	mutex_lock(&mitsubishi->reset_mutex);

	mitsubishi->hw_initialized = 0;
	init_completion(&mitsubishi->init_done);

	pkt.header = MITSUBISHI_REQ_RESET;
	pkt.data_len = 1;
	pkt.data[0] = (pkt.header + pkt.data_len) & 0x7F;

	rc = mitsubishi_command(mitsubishi, &pkt, NULL, 100);
	if (rc)
		goto out;

	timeleft = wait_for_completion_timeout(&mitsubishi->init_done,
					       msecs_to_jiffies(2000));
	if (timeleft == 0) {
		printk(KERN_WARNING
		       "mitsubishi: timeout waiting for device to initialize\n");
		rc = -ETIMEDOUT;
		goto out;
	}

	mitsubishi->hw_initialized = 1;

out:
	mutex_unlock(&mitsubishi->reset_mutex);
	return rc;
}

static int mitsubishi_get_info(struct mitsubishi *mitsubishi)
{
	struct mitsubishi_packet pkt;
	struct mitsubishi_packet resp;
	int rc = 0;
	
	if (mitsubishi->debug_flags >= MITSUBISHI_DEBUG_MSG_LOG)
	  printk(KERN_INFO "Touch: device get info\n");

	pkt.header = MITSUBISHI_REQ_VERSION;
	pkt.data_len = 1;
	pkt.data[0] = (pkt.header + pkt.data_len) & 0x7F;

	rc = mitsubishi_command(mitsubishi, &pkt, &resp, 100);
	if (rc)
		goto out;

	mitsubishi->info.version.major = resp.data[0];
	mitsubishi->info.version.minor = resp.data[1];

out:
	return rc;
}

static int mitsubishi_set_samprate(struct mitsubishi *mitsubishi, unsigned char semplrate)
{
	struct mitsubishi_packet pkt;
	struct mitsubishi_packet resp;
	int rc = 0;
	
	if (semplrate < 0x02) semplrate = 0x02;
	else if (semplrate > 0x64) semplrate = 0x64;

	pkt.header = MITSUBISHI_SET_SAMPRATE;
	pkt.data_len = 2;
	pkt.data[0] = semplrate;
	pkt.data[1] = (pkt.header + pkt.data_len + pkt.data[0]) & 0x7F;

	rc = mitsubishi_command(mitsubishi, &pkt, &resp, 100);
	
	mitsubishi->semp_rate = semplrate;
	
	if (mitsubishi->debug_flags >= MITSUBISHI_DEBUG_MSG_LOG)
	  printk(KERN_INFO "Touch: set sempl rate = 0x%02X (%03i ms)\n", semplrate, semplrate);
	
	return rc;
}

static ssize_t mitsubishi_show_semp_rate(struct device *dev,
				 struct device_attribute *attr, char *buf)
{
	struct serio *serio = to_serio_port(dev);
	struct mitsubishi *mitsubishi = serio_get_drvdata(serio);

	return sprintf(buf, "%lX\n", mitsubishi->semp_rate);
}

static ssize_t mitsubishi_store_semp_rate(struct device *dev,
				  struct device_attribute *attr,
				  const char *buf, size_t count)
{
	struct serio *serio = to_serio_port(dev);
	struct mitsubishi *mitsubishi = serio_get_drvdata(serio);

	sscanf(buf, "%lX", &mitsubishi->semp_rate);
	mitsubishi_set_samprate(mitsubishi, mitsubishi->semp_rate);
	return count;
}

static DEVICE_ATTR(semp_rate, S_IRUGO | S_IWUSR, mitsubishi_show_semp_rate, mitsubishi_store_semp_rate);

static DEVICE_ATTR(version, S_IRUGO, mitsubishi_show_version, NULL);
static DEVICE_ATTR(reset, S_IWUSR, NULL, mitsubishi_store_reset);
static DEVICE_ATTR(debug_flags, S_IRUGO | S_IWUSR, mitsubishi_show_debug, mitsubishi_store_debug);

static struct attribute *mitsubishi_attrs[] = {
	&dev_attr_version.attr,
	&dev_attr_reset.attr,
	&dev_attr_debug_flags.attr,
	&dev_attr_semp_rate.attr,
	NULL
};

static const struct attribute_group mitsubishi_attr_group = {
	.attrs	= mitsubishi_attrs,
};


static int mitsubishi_setup(struct mitsubishi *mitsubishi)
{
	struct input_dev *dev = mitsubishi->dev;
	unsigned long timeleft;
	int rc = 0;
	
	if (mitsubishi->debug_flags > MITSUBISHI_DEBUG_MSG_LOG)
		printk(KERN_INFO "Touch: MITSUBISHI UART driver setup device\n");

	timeleft = wait_for_completion_timeout(&mitsubishi->init_done,
					       msecs_to_jiffies(2000));
	if (timeleft == 0) {
		rc = mitsubishi_sw_reset(mitsubishi);
		if (rc)
			goto out;
	}
	
	rc = mitsubishi_set_samprate(mitsubishi, 0x0F);
	if (rc)
		goto out;

	rc = mitsubishi_get_info(mitsubishi);
	if (rc)
		goto out;

	dev->id.version = (u16)mitsubishi->info.version.major << 8 |
				mitsubishi->info.version.minor;

out:
	return rc;
}

/*
 * mitsubishi_disconnect() is the opposite of mitsubishi_connect()
 */

static void mitsubishi_disconnect(struct serio *serio)
{
	struct mitsubishi *mitsubishi = serio_get_drvdata(serio);

	
	printk(KERN_INFO "Touch: MITSUBISHI UART driver disconnect event.\n");
	input_get_device(mitsubishi->dev);
	input_unregister_device(mitsubishi->dev);
	sysfs_remove_link(&serio->dev.parent->kobj, "mitsubishi");
	sysfs_remove_group(&serio->dev.kobj, &mitsubishi_attr_group);
	serio_close(serio);
	serio_set_drvdata(serio, NULL);
	input_put_device(mitsubishi->dev);
	kfree(mitsubishi);
}

/*
 * mitsubishi_connect() is the routine that is called when someone adds a
 * new serio device that supports MITSUBISHI protocol and registers it as
 * an input device.
 */

static int mitsubishi_connect(struct serio *serio, struct serio_driver *drv)
{
	struct mitsubishi *mitsubishi;
	struct input_dev *input_dev;
	int err;
	
	printk(KERN_INFO "Touch: MITSUBISHI UART driver start connect event.\n");
	
	mitsubishi = kzalloc(sizeof(struct mitsubishi), GFP_KERNEL);
	
	if (!mitsubishi) {
		err = -ENOMEM;
		goto fail1;
	}
	
	mitsubishi->debug_flags = MITSUBISHI_DEBUG_DEFAULT;

	input_dev = input_allocate_device();
	if (!input_dev) {
		err = -ENOMEM;
		goto fail2;
	}

	mitsubishi->serio = serio;
	mitsubishi->dev = input_dev;
	mutex_init(&mitsubishi->reset_mutex);
	mutex_init(&mitsubishi->cmd_mutex);
	init_completion(&mitsubishi->init_done);
	init_completion(&mitsubishi->cmd_done);
	snprintf(mitsubishi->phys, sizeof(serio->phys), "%s/input0", serio->phys);

	input_dev->name = "MITSUBISHI UART Touch";
	input_dev->phys = mitsubishi->phys;
	input_dev->id.bustype = BUS_RS232;
	input_dev->id.vendor = SERIO_MITSUBISHI;
	input_dev->id.product = 0x0000;
	input_dev->id.version = 0x0000;
	input_dev->dev.parent = &serio->dev;
	input_set_capability(input_dev, EV_ABS, ABS_MT_POSITION_X);
	input_set_capability(input_dev, EV_ABS, ABS_MT_POSITION_Y);
	input_set_capability(input_dev, EV_ABS, ABS_X);
	input_set_capability(input_dev, EV_ABS, ABS_Y);
	input_set_capability(input_dev, EV_KEY, BTN_TOUCH);
	input_set_abs_params(input_dev, ABS_MT_TRACKING_ID, 0, MITSUBISHI_MAX_TRKID, 0, 0);
	input_set_abs_params(input_dev, ABS_MT_POSITION_X, 0, MITSUBISHI_MAX_X, 0, 0);
	input_set_abs_params(input_dev, ABS_MT_POSITION_Y, 0, MITSUBISHI_MAX_Y, 0, 0);
	input_set_abs_params(input_dev, ABS_X, 0, MITSUBISHI_MAX_X, 0, 0);
	input_set_abs_params(input_dev, ABS_Y, 0, MITSUBISHI_MAX_Y, 0, 0);

	serio_set_drvdata(serio, mitsubishi);
	err = serio_open(serio, drv);
	if (err)
		goto fail3;

	err = mitsubishi_setup(mitsubishi);
	if (err)
		goto fail4;

	err = sysfs_create_group(&serio->dev.kobj, &mitsubishi_attr_group);
	if (err)
		goto fail4;

	err = sysfs_create_link(&serio->dev.parent->kobj, &serio->dev.kobj, "mitsubishi");
	if (err)
		goto fail5;

	err = input_register_device(mitsubishi->dev);
	if (err)
		goto fail6;

	if (mitsubishi->debug_flags > MITSUBISHI_DEBUG_MSG_SATE)
		printk(KERN_INFO "Touch: MITSUBISHI UART driver connected.\n");

	return 0;

fail6:
	sysfs_remove_link(&serio->dev.parent->kobj, "mitsubishi");
fail5:
	sysfs_remove_group(&serio->dev.kobj, &mitsubishi_attr_group);
fail4:
	serio_close(serio);
fail3:
	serio_set_drvdata(serio, NULL);
	input_free_device(input_dev);
fail2:
	kfree(mitsubishi);
fail1:
	return err;
}

static struct serio_device_id mitsubishi_serio_ids[] = {
	{
		.type	= SERIO_RS232,
		.proto	= SERIO_MITSUBISHI,
		.id	= SERIO_ANY,
		.extra	= SERIO_ANY,
	},
	{ 0 }
};

MODULE_DEVICE_TABLE(serio, mitsubishi_serio_ids);

static struct serio_driver mitsubishi_drv = {
	.driver		= {
		.name	= "mitsubishi",
	},
	.description	= DRIVER_DESC,
	.id_table	= mitsubishi_serio_ids,
	.interrupt	= mitsubishi_interrupt,
	.connect	= mitsubishi_connect,
	.disconnect	= mitsubishi_disconnect,
};

static int __init mitsubishi_init(void)
{
	
	printk(KERN_INFO "Touch: MITSUBISHI UART driver registred.\n");
	return serio_register_driver(&mitsubishi_drv);
}

static void __exit mitsubishi_exit(void)
{
	printk(KERN_INFO "Touch: MITSUBISHI UART driver unregistred.\n");
	serio_unregister_driver(&mitsubishi_drv);
}

module_init(mitsubishi_init);
module_exit(mitsubishi_exit);
